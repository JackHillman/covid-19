const fetch = require('node-fetch')
const graphite = require('graphite')
const client = graphite.createClient(`plaintext://localhost:2003/`)

const scheme = process.env.APP_SCHEME || `http`
const port = process.env.APP_PORT || 8000
const host = process.env.APP_HOST || `localhost`
const uri = process.env.APP_URI || ``

new Promise(async () => {
  const data = await fetch(`${scheme}://${host}:${port}${uri}`).then(res => res.json())

  const prefix = 'covid.cases.australia'

  const formattedData = data.map(([label, value]) => {
    const name = label.value.toLowerCase().replace(/\s+/gmi, '-')

    return [`${prefix}.${name}`, value.value]
  })

  formattedData.forEach(([key, value]) => {
    client.write({ [key]: value }, (err) => {
      if (err) {
        console.error(err)
      }
    })
  })
})
