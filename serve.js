const app = require('express')()
const pull = require('./pull-data')

app.get('/', async (req, res) => {
  res.send(await pull())
})

const port = process.env.APP_PORT || 8000

console.log(`App listening on port ${port}`)
app.listen(port)

