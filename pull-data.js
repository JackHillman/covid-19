const fetch = require('node-fetch')
const JSDOM = require('jsdom').JSDOM

const url = `https://www.health.gov.au/news/health-alerts/novel-coronavirus-2019-ncov-health-alert/coronavirus-covid-19-current-situation-and-case-numbers`

module.exports = async () => {
  const response = await fetch(url).then(res => res.text())
  const dom = new JSDOM(response)
  const document = dom.window.document

  const headers = ['location', 'confirmed-cases']
  const rows = Array.from(document.querySelectorAll('.health-table__responsive tbody tr:not(:first-child)'))

  const data = rows.map((tr) => {
    const row = []

    Array.from(tr.querySelectorAll('td')).forEach((td, i) => {
      const name = headers[i]
      let value = td.textContent.trim().replace(/[^A-z 0-9]/gmi, '')

      if (name === 'confirmed-cases') {
        value = parseInt(value)
      }

      row[i] = { name, value }
    })

    return row
  })

  return data
}
